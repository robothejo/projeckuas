package com.example.projeckuas;

public class ModalDatabase {
    private String _id, _tanggal, _ayat1, _surah1, _ayat2, _surah2;

    public  ModalDatabase (String id, String tanggal, String  ayat1, String surah1, String ayat2, String surah2){
        this._id = id;
        this._tanggal = tanggal;
        this._ayat1 = ayat1;
        this._surah1 = surah1;
        this._ayat2 = ayat2;
        this._surah2 = surah2;
    }

    public ModalDatabase(){

    }

    public void set_id (String id){
        this._id = id;
    }

    public String get_id (){
        return this._id;
    }

    public void set_tanggal (String tanggal){
        this._tanggal = tanggal;
    }

    public String get_tanggal (){
        return this._tanggal;
    }

    public void set_ayat1 (String ayat1){
        this._ayat1 = ayat1;
    }

    public String get_ayat1 (){
        return this._ayat1;
    }

    public void set_surah1 (String surah1){
        this._surah1 = surah1;
    }

    public String get_surah1 (){
        return this._surah1;
    }

    public void set_ayat2 (String ayat2){
        this._ayat2 = ayat2;
    }

    public String get_ayat2 (){
        return this._ayat2;
    }

    public void set_surah2 (String surah2){
        this._surah2 = surah2;
    }

    public String get_surah2 (){
        return this._surah2;
    }
}
