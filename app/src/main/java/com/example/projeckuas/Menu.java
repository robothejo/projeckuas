package com.example.projeckuas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Menu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }


    public void Catatan(View view) {
        Intent intent = new Intent(Menu.this, CatatanActivity.class);
        startActivity(intent);
    }
    public void MBA(View view) {
        Intent intent = new Intent(Menu.this, MbaActivity.class);
        startActivity(intent);
    }

    public void SHALAT(View view) {
        Intent intent = new Intent(Menu.this, ShalatActivity.class);
        startActivity(intent);
    }
}

