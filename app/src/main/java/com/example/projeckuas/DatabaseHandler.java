package com.example.projeckuas;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHandler extends SQLiteOpenHelper {

    private static int Database_Version = 1;
    private static final String Database_Name = "db_catatan";

    private static final String tb_mba = "tb_mba";

    private static final String tb_mba_id = "id";
    private static final String tb_mba_tanggal = "tanggal";
    private static final String tb_mba_ayat1 = "ayat1";
    private static final String tb_mba_surah1 = "surah1";
    private static final String tb_mba_ayat2 = "ayat2";
    private static final String tb_mba_surah2 = "surah2";

    private static final String Create_Table_mba = "CREATE TABLE " + tb_mba + "("
            + tb_mba_id + " INTEGER PRIMARY KEY ,"
            + tb_mba_tanggal + " TEXT ,"
            + tb_mba_ayat1 +  " TEXT ,"
            + tb_mba_surah1 + " TEXT ,"
            + tb_mba_ayat2 + " TEXT ,"
            + tb_mba_surah2 + " TEXT " + ")";


    public DatabaseHandler(Context context){
        super(context, Database_Name, null, Database_Version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Create_Table_mba);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void CreateMba (ModalDatabase mdNotif){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(tb_mba_id, mdNotif.get_id());
        values.put(tb_mba_tanggal, mdNotif.get_tanggal());
        values.put(tb_mba_ayat1, mdNotif.get_ayat1());
        values.put(tb_mba_surah1, mdNotif.get_surah1());
        values.put(tb_mba_ayat2, mdNotif.get_ayat2());
        values.put(tb_mba_surah2, mdNotif.get_surah2());
        db.insert(tb_mba, null, values);
        db.close();
    }

}
