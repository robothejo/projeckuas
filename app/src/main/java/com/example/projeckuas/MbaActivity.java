package com.example.projeckuas;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MbaActivity extends AppCompatActivity {

    String[] daftar;
    ListView ListView01;
    protected Cursor cursor;
    DataHelper dbcenter;
    private static MbaActivity mb;

    public static MbaActivity getMb() {
        return mb;
    }

    public static void setMb(MbaActivity mb) {
        MbaActivity.mb = mb;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mba);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab2);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MbaActivity.this, TambahMba.class);
                startActivity(intent);
            }
        });

        setMb(this);
        dbcenter = new DataHelper(this);
        RefreshList();

    }


    public void RefreshList() {
        SQLiteDatabase db = dbcenter.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM mba", null);
        daftar = new String[cursor.getCount()];
        cursor.moveToFirst();
        for (int cc = 0; cc < cursor.getCount(); cc++) {
            cursor.moveToPosition(cc);
            daftar[cc] = cursor.getString(1).toString();
        }
        ListView01 = (ListView) findViewById(R.id.listview1);
//        ListView01.setAdapter(new ArrayAdapter(this, android.R.layout.simple_list_item_1, daftar));
//        ListView01.setSelected(true);
//        ListView01.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//
//            public void onItemClick(AdapterView arg0, View arg1, int arg2, long arg3) {
//                final String selection = daftar[arg2]; //.getItemAtPosition(arg2).toString();
//                final CharSequence[] dialogitem = {"Lihat ", "Update ", "Hapus "};
//                AlertDialog.Builder builder = new AlertDialog.Builder(MbaActivity.this);
//                builder.setTitle("Pilihan");
//                builder.setItems(dialogitem, new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int item) {
//                        switch (item) {
//                            case 0:
//                                Intent i = new Intent(getApplicationContext(), LihatMba.class);
//                                i.putExtra("tanggal", selection);
//                                startActivity(i);
//                                break;
//                            case 1:
//                                Intent in = new Intent(getApplicationContext(), UpdateMba.class);
//                                in.putExtra("tanggal", selection);
//                                startActivity(in);
//                                break;
//                            case 2:
//                                SQLiteDatabase db = dbcenter.getWritableDatabase();
//                                db.execSQL("delete from mba where tanggal = '" + selection + "'");
//                                RefreshList();
//                                break;
//                        }
//                    }
//                });
//                builder.create().show();
//            }
//        });
//        ((ArrayAdapter) ListView01.getAdapter()).notifyDataSetInvalidated();
    }


}
