package com.example.projeckuas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class TambahMba extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    private DatabaseHandler db;
    private EditText Etanggal, Eayat1, Eayat2, Esurah1, Esurah2;
    private String stanggal, sayat1, sayat2, ssurah1, ssurah2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_mba);

        db = new DatabaseHandler(this);

        Etanggal = (EditText) findViewById(R.id.tanggal);
        Eayat1 = (EditText) findViewById(R.id.ayat1);
        Esurah1 = (EditText) findViewById(R.id.surah1);
        Eayat2 = (EditText) findViewById(R.id.ayat2);
        Esurah2 = (EditText) findViewById(R.id.surah2);

        Button btnCreate = (Button) findViewById(R.id.btn_simpan);
        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                stanggal = String.valueOf((Etanggal.getText()));
                sayat1 = String.valueOf((Eayat1.getText()));
                ssurah1 = String.valueOf((Esurah1.getText()));
                sayat2 = String.valueOf((Eayat2.getText()));
                ssurah2 = String.valueOf((Esurah2.getText()));

                if (stanggal.equals("")){
                    Etanggal.requestFocus();
                    Toast.makeText(TambahMba.this, "Silahkan isi tanggal", Toast.LENGTH_SHORT).show();
                }else if (sayat1.equals("")) {
                    Eayat1.requestFocus();
                    Toast.makeText(TambahMba.this, "Silahkan isi ayat", Toast.LENGTH_SHORT).show();
                }else if (ssurah1.equals("")) {
                    Esurah1.requestFocus();
                    Toast.makeText(TambahMba.this, "Silahkan isi surah", Toast.LENGTH_SHORT).show();
                }else  if (sayat2.equals("")) {
                    Eayat2.requestFocus();
                    Toast.makeText(TambahMba.this, "Silahkan isi ayat", Toast.LENGTH_SHORT).show();
                }else if (ssurah2.equals("")) {
                    Esurah2.requestFocus();
                    Toast.makeText(TambahMba.this, "Silahkan isi surah", Toast.LENGTH_SHORT).show();
                }else {
                    Etanggal.setText("");
                    Eayat1.setText("");
                    Esurah1.setText("");
                    Eayat2.setText("");
                    Esurah2.setText("");
                    Toast.makeText(TambahMba.this, "Data telah diisi", Toast.LENGTH_SHORT).show();
                    db.CreateMba(new ModalDatabase(null, stanggal, sayat1, ssurah1, sayat2, ssurah2));
                }

            }
        });


    }



    public void batal(View view) {
        Intent intent = new Intent(TambahMba.this, MbaActivity.class);
        startActivity(intent);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
