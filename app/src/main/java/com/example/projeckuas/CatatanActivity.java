package com.example.projeckuas;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class CatatanActivity extends AppCompatActivity {
    String[] daftar;
    ListView ListView01;
    Menu menu;
    protected Cursor cursor;
    DataHelper dbcenter;
    public static CatatanActivity ma;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catatan);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab1); 
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(CatatanActivity.this, TambahCatatan.class);
                    startActivity(intent);
                }


            });

        ma = this;
        dbcenter = new DataHelper(this);
        RefreshList();

    }

    private void RefreshList() {
    }

}
